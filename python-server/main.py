from http.server import SimpleHTTPRequestHandler, HTTPServer
from platform import node

_HOSTNAME = node()


class HostnameHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200, 'OK')
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(f'Python container {_HOSTNAME}'.encode('utf-8'))


server_address = ('', 8080)
httpd = HTTPServer(server_address, HostnameHandler)
httpd.serve_forever()